<?php

namespace App\Command;

use App\Services\JobsDownloader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class JobsGetCommand extends Command
{
    protected static $defaultName = 'app:jobs:get';

    /**
     * @var JobsDownloader
     */
    protected $jobsDownloader;

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('url', InputArgument::OPTIONAL, 'Enter URL to get one item result.')
        ;
    }

    /**
     * @required
     * @param JobsDownloader $jobsDownloader
     */
    public function init(JobsDownloader $jobsDownloader)
    {
        $this->jobsDownloader = $jobsDownloader;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jobs = $this->jobsDownloader->parseJobs();

        $table = new Table($output);
        $table->setHeaders(['Job position', 'Location', 'Description', 'Exp. level by regex', 'Exp. years by regex', 'Exp. level by ML']);

        $rows = [];

        foreach ($jobs as $job) {

            $rows[] = [
                $job['title'],
                $job['location'],
                substr( $job['text'], 0 , 30 ) . '...',
                $job['experience_level_regex'],
                $job['experience_years_regex'],
                $job['experience_level_ml']['winner']
            ];

            /**
             * Uncomment to see Job URL
             */
//            $rows[] = [new TableCell($job['url'], ['colspan' => 6])];
//            $rows[] = new TableSeparator();
        }

        $table
            ->setRows( $rows )
            ->render()
        ;
    }
}
