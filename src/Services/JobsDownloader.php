<?php

declare(strict_types=1);

namespace App\Services;

use App\Stage\AppendExperienceLevelByMl;
use App\Stage\AppendExperienceLevelByRegex;
use App\Stage\AppendExperienceYearsByRegex;
use App\Stage\AppendJobsByJsonResponse;
use App\Stage\AppendTextBySpotifyJob;
use App\Stage\FixHtmlEntities;
use App\Stage\FixSpotifyJobs;
use App\Stage\FixStripSpaces;
use League\Pipeline\Pipeline;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobsDownloader {

    public const SPOTIFY_JOBS_DEFINITION = [
        'service' => 'spotify',
        'jobs_response_path' => 'data.items',
        'method' => 'POST',
        'url' => 'https://www.spotifyjobs.com/wp-admin/admin-ajax.php',
        'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8'
        ],
        'body' => [
            'action' => 'get_jobs',
            'pageNr' => 1,
            'perPage' => 30,
            'featuredJobs' => '',
            'category' => 0,
            'location' => 0,
            'search' => '',
            'locations[]' => 'sweden'
        ]
    ];

    public const ACTIVE_DEFINITIONS = [self::SPOTIFY_JOBS_DEFINITION];

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function parseJobs()
    {
        $jobs = [];

        foreach(self::ACTIVE_DEFINITIONS as $definition)
        {
            switch ($definition['service']) {
                case 'spotify':

                    $definitionJobs = [];

                    $pipeline = (new Pipeline())
                        ->pipe(new AppendJobsByJsonResponse($definition))
                        ->pipe(new FixSpotifyJobs)
                    ;
                    $definitionJobs = $pipeline->process( $definitionJobs );

                    foreach($definitionJobs as $job) {

                        $pipeline = (new Pipeline())
                            ->pipe(new AppendTextBySpotifyJob)
                            ->pipe(new FixHtmlEntities(['title']))
                            ->pipe(new FixStripSpaces(['text']))
                            ->pipe(new AppendExperienceLevelByRegex(['title', 'text']))
                            ->pipe(new AppendExperienceYearsByRegex(['title', 'text']))
                            ->pipe(new AppendExperienceLevelByMl($this->container->getParameter('kernel.project_dir')))
                        ;
                        $jobs[] = $pipeline->process( $job );
                    }
                break;
            }
        }
        return $jobs;
    }
}

