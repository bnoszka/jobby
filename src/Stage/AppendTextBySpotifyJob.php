<?php

declare(strict_types=1);

namespace App\Stage;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class AppendTextBySpotifyJob
{
    private $crawler;
    private $httpClient;

    public function __construct()
    {
        $this->crawler = new Crawler();
        $this->httpClient = HttpClient::create();
    }

    public function __invoke($job)
    {
        $this->crawler->addHtmlContent( $this->httpClient->request('GET', $job['url'] )->getContent() );

        $job['title'] = $this->crawler->filterXPath('//h1')->text();
        $job['text'] = $this->crawler->filterXPath('//div[@class="column-inner"]')->text();

        return $job;
    }
}
