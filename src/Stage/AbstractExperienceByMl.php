<?php

declare(strict_types=1);

namespace App\Stage;

use Phpml\Dataset\ArrayDataset;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Metric\Accuracy;
use Phpml\Tokenization\WordTokenizer;
use Phpml\CrossValidation\StratifiedRandomSplit;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Classification\SVC;
use Phpml\SupportVectorMachine\Kernel;

abstract class AbstractExperienceByMl {

    protected $rootDir;
    protected $vectorizer;
    protected $tfIdfTransformer;
    protected $classifier;

    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
        $this->vectorizer = new TokenCountVectorizer(new WordTokenizer());
        $this->tfIdfTransformer = new TfIdfTransformer();
    }

    protected function predict($dataset, $text)
    {
        $samples = [];
        foreach ($dataset->getSamples() as $sample) {
            $samples[] = $sample[0];
        }

        $this->vectorizer->fit($samples);

        $this->vectorizer->transform($samples);
        $this->tfIdfTransformer->fit($samples);
        $this->tfIdfTransformer->transform($samples);
        $dataset = new ArrayDataset($samples, $dataset->getTargets());
        $randomSplit = new StratifiedRandomSplit($dataset, 0.1);

        $this->classifier = new SVC(Kernel::RBF, 10000);
        $this->classifier->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());

        $newSample = [$text];
        $this->vectorizer->transform($newSample);
        $this->tfIdfTransformer->transform($newSample);

        $results = [];
        foreach($randomSplit->getTestSamples() as $key => $testSample)
        {
            $results['accuracy'][$randomSplit->getTestLabels()[$key]] = Accuracy::score($newSample[0], $testSample, false);
        }

        $results['winner'] = $this->findWinner( $results['accuracy'] );

        return $results;
    }

    private function findWinner(array $accuracies): string
    {
        if(count($accuracies) < 1)
        {
            return '';
        }
        asort($accuracies);

        $accuracies = array_flip($accuracies);
        $accuracies = array_reverse($accuracies);

        return $accuracies[0];
    }

}

