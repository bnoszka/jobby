<?php

declare(strict_types=1);

namespace App\Stage;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PropertyAccess\PropertyAccess;

class AppendJobsByJsonResponse
{
    private $definition;
    private $httpClient;

    public function __construct($definition)
    {
        $this->definition = $definition;
        $this->httpClient = HttpClient::create();
    }

    public function __invoke($job)
    {
        $response = $this->httpClient->request($this->definition['method'], $this->definition['url'], [
            'headers' => $this->definition['headers'],
            'body' => $this->definition['body']
        ]);

        $content = json_decode( $response->getContent() );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        return $propertyAccessor->getValue($content, $this->definition['jobs_response_path']);
    }
}
