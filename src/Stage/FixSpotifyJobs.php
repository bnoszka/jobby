<?php

declare(strict_types=1);

namespace App\Stage;

class FixSpotifyJobs
{
    public function __invoke($jobsToFix)
    {
        $jobs = [];
        foreach($jobsToFix as $jobToFix)
        {
            $job['title'] = $jobToFix->title;
            $job['url'] = $jobToFix->url;
            $job['location'] = implode(', ', array_map(function($location) { return $location->name; }, $jobToFix->locations));
            $jobs[] = $job;
        }
        return $jobs;
    }
}
