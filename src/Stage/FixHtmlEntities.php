<?php

declare(strict_types=1);

namespace App\Stage;

class FixHtmlEntities
{
    private $fields;

    public function __construct(array $fields = [])
    {
        $this->fields = $fields;
    }

    public function __invoke($job)
    {
        foreach ($this->fields as $field)
        {
            $job[$field] = html_entity_decode($job[$field]);
        }
        return $job;
    }
}
