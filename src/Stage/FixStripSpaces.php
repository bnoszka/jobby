<?php

declare(strict_types=1);

namespace App\Stage;

class FixStripSpaces
{
    private $fields;

    public function __construct(array $fields = [])
    {
        $this->fields = $fields;
    }

    public function __invoke($job)
    {
        foreach ($this->fields as $field)
        {
            $content = $job[$field];
            $content = trim(preg_replace('/\s+/', ' ', $content));
            $job[$field] = $content;
        }
        return $job;
    }
}
