<?php

declare(strict_types=1);

namespace App\Stage;

use Phpml\Dataset\CsvDataset;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\FeatureExtraction\TfIdfTransformer;

ini_set('memory_limit', '-1');

class AppendExperienceLevelByMl extends AbstractExperienceByMl {

    public function __invoke($job)
    {
        $dataset = new CsvDataset( $this->rootDir . '/data/experience_level.csv', 1, false);
        $this->vectorizer = new TokenCountVectorizer(new WordTokenizer());
        $this->tfIdfTransformer = new TfIdfTransformer();

        $job['experience_level_ml'] = $this->predict($dataset, $job['text']);

        return $job;
    }

}

