<?php

declare(strict_types=1);

namespace App\Stage;

class AppendExperienceYearsByRegex
{
    private $fields;

    public function __construct(array $fields = [])
    {
        $this->fields = $fields;
    }

    public function __invoke($job)
    {
        $content = implode(' ', array_map(function ($field) use ($job) { return $job[$field]; } ,$this->fields) );

        $matches = [];
        preg_match_all('/(\d\+ years|\d years)/', mb_strtolower( $content ), $matches);

        $job['experience_years_regex'] = $this->flatten($matches);

        return $job;
    }

    private function flatten(array $array) {
        $return = array();
        array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
        return implode(', ' , array_unique( $return ) );
    }
}
