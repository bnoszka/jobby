#### Preface

What you need:

1. PHP on your local (tested with 7.3),
2. Composer on your local.

#### Installation


1. Get repository using CLI

   ```
   git clone https://gitlab.com/bnoszka/jobby.git _jobby
   ```

2. Move to directory

   ```
   cd _jobby
   ```
   
3. Download vendors

   ```
   composer install
   ```

4. Run command to find jobs

   ```
   php bin/console app:job:get
   ```
   
5. Change jobs result per page in file

   ```
   src/Services/JobsDownloader
   ```

#### Preview

![](PREVIEW.png)
